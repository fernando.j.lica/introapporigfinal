import React, { Component } from 'react'
import { StyleSheet, View, Text, TextInput, Button, TouchableOpacity, FlatList, ScrollView } from 'react-native';

const ShoppingItems = ({ id, name }) => (
    <View style={styles.shoppingItems}>
        <Text style={{color: "#ffff"}}> {id} - {name} </Text>
    </View>
  );

export class SecondaryMenu extends Component {
    
    constructor(props) {
        super(props);

        this.state = {
            itemId: '',
            ItemName: ''
        }

        this.addItem = this.addItem.bind(this)
    }

    usersList = [];

    data = [
        {
            id: '1',
            name: 'Açucar',
        },
        {
            id: '2',
            name: 'Adocante',
        },
        {
            id: '3',
            name: 'Arroz',
        },
        {
            id: '4',
            name: 'Atum',
        },
        {
            id: '5',
            name: 'Azeite',
        },
        {
            id: '6',
            name: 'Batatas',
        },
        {
            id: '1',
            name: 'Açucar',
        },
        {
            id: '2',
            name: 'Adocante',
        },
        {
            id: '3',
            name: 'Arroz',
        },
        {
            id: '4',
            name: 'Atum',
        },
        {
            id: '5',
            name: 'Azeite',
        },
        {
            id: '6',
            name: 'Batatas',
        },
        {
            id: '1',
            name: 'Açucar',
        },
        {
            id: '2',
            name: 'Adocante',
        },
        {
            id: '3',
            name: 'Arroz',
        },
        {
            id: '4',
            name: 'Atum',
        },
        {
            id: '5',
            name: 'Azeite',
        },
        {
            id: '6',
            name: 'Batatas',
        },
        {
            id: '1',
            name: 'Açucar',
        },
        {
            id: '2',
            name: 'Adocante',
        },
        {
            id: '3',
            name: 'Arroz',
        },
        {
            id: '4',
            name: 'Atum',
        },
        {
            id: '5',
            name: 'Azeite',
        },
        {
            id: '6',
            name: 'Batatas',
        },
        {
            id: '1',
            name: 'Açucar',
        },
        {
            id: '2',
            name: 'Adocante',
        },
        {
            id: '3',
            name: 'Arroz',
        },
        {
            id: '4',
            name: 'Atum',
        },
        {
            id: '5',
            name: 'Azeite',
        },
        {
            id: '6',
            name: 'Batatas',
        },

      ];

      addItem(){
        
        const { itemId, itemName } = this.state; // Novo User
        this.data.push({id: this.state.itemId, name: this.state.itemName})

        this.forceUpdate()

        // fireBase
    }


    render() {
        console.log('SecondaryMenu PARAMS: ', this.props.route.params)
        this.usersList = this.props.route.params
        return (
            <View style={styles.container}>
                
                <ScrollView>
                    <Text style={styles.textTitle}>SecondaryMenu Screen</Text>

                    {this.data.map((item) => {
                        return (
                        <View style={styles.shoppingItems}>
                            <Text>{item.name}</Text>
                        </View>
                        );
                    })}




                    {/* <TextInput style={styles.inputField}
                        placeholder="Item Id"
                        onChangeText={(itemId) => this.setState({ itemId })}
                    />
                    <TextInput style={styles.inputField}
                        placeholder="Item Name"
                        onChangeText={(itemName) => this.setState({ itemName })}
                    />

                    <TouchableOpacity style={styles.button} onPress={() => this.addItem()}>
                            <Text style={styles.text}>Add</Text>
                    </TouchableOpacity> */}

                    <TouchableOpacity style={styles.button} onPress={() => this.props.navigation.navigate("Landing", this.props.route.params)}>
                            <Text style={styles.text}>Back</Text>
                    </TouchableOpacity>

                </ScrollView>  

              
                
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      justifyContent: "center",
      alignItems: "center",
    },
    
   
    button: {
      justifyContent: "center",
      alignItems: "center",
      padding: 10,
      borderRadius: 5,
      marginBottom: 5,
      width: 100,
      backgroundColor: "#3fa33c",
    },
  
    text: {
        color: "#ffff",
        fontWeight: "bold"
    },

    textTitle: {
        marginVertical: 50,
        fontWeight: "bold"
    },

    shoppingItems: {
        flexDirection: "row",
        padding: 10,
        borderRadius: 5,
        marginBottom: 5,
        width: 150,
        backgroundColor: "#3fa33c",
    },

    shoppingItemsContainer: {
        flex: 1,
        marginTop: 25
      },

    inputField: {
        marginVertical: 5,
        padding: 5,
        borderColor: '#000000',
        width: 100,
        borderWidth: 1
    },
  
  });

export default SecondaryMenu