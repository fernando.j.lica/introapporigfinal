import React, { Component } from 'react'
import { StyleSheet, Text, View, Button, TouchableOpacity } from 'react-native'

export class Landing extends Component{

  constructor(props) {
    super(props);
  }

  render(){ 
    console.log('LANDING PARAMS: ', this.props.route.params)
    return (
      <View style={styles.container}>
          <TouchableOpacity style={styles.button} onPress={() => this.props.navigation.navigate("Register", this.props.route.params)}>
                <Text style={styles.text}>Register</Text>
          </TouchableOpacity>

          <TouchableOpacity style={styles.button} onPress={() => this.props.navigation.navigate("Login", this.props.route.params)}>
                <Text style={styles.text}>Login</Text>
          </TouchableOpacity>
      </View>
    )
  }
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  
 
  button: {
    justifyContent: "center",
    alignItems: "center",
    padding: 10,
    borderRadius: 5,
    marginTop: 15,
    width: 100,
    backgroundColor: "#3fa33c",
  },

  text: {
    color: "#ffff",
    fontWeight: "bold"
  },

});

export default Landing
