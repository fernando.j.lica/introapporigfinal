import React, { Component } from 'react'
import { StyleSheet, View, Text, TextInput, Button, TouchableOpacity } from 'react-native';

export class Login extends Component {

    constructor(props) {
        super(props);

        this.state = {
            name: '',
            email: '',
            password: ''
        }

        //depois do render
        this.onSignIn = this.onSignIn.bind(this)
         
    }

    usersList = []
    

    //depois do render
    onSignIn(){
        
        const { name, email, password } = this.state; // Novo User

        if(this.usersList === undefined){
            this.usersList = [{name: 'user1', email: 'user1@user1.com', password: 'user1pass'}]
        }

        const isFound = this.usersList.some( user => { 
                if (user.email == email) {
                    return true 
                }
                else{
                    return false
                }
        });

        if(!isFound){console.log('User not found')}
        else{console.log('Login sucessfull'), this.props.navigation.navigate("MainMenu", this.props.route.params)}
        

        // fireBase
    }


    render() {
        console.log('LOGIN  PARAMS: ', this.props.route.params)
        this.usersList = this.props.route.params
        return (
            <View style={styles.container}>
                <Text style={styles.textTitle}>Login Screen</Text>

                
                {/* <TextInput
                    placeholder="name"
                    onChangeText={(name) => this.setState({ name })}
                /> */}
                
                <TextInput style={styles.inputField}
                    placeholder="email"
                    onChangeText={(email) => this.setState({ email })}
                />
                <TextInput style={styles.inputField}
                    placeholder="password"
                    secureTextEntry={true}
                    onChangeText={(password) => this.setState({ password })}
                />

                <TouchableOpacity style={styles.button} onPress={() => this.onSignIn()}>
                        <Text style={styles.text}>Sign In</Text>
                </TouchableOpacity>

                <TouchableOpacity style={styles.button} onPress={() => this.props.navigation.navigate("Landing", this.props.route.params)}>
                        <Text style={styles.text}>Back</Text>
                </TouchableOpacity>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      justifyContent: "center",
      alignItems: "center",
    },
    
    button: {
      justifyContent: "center",
      alignItems: "center",
      padding: 10,
      borderRadius: 5,
      marginTop: 15,
      width: 100,
      backgroundColor: "#3fa33c",
    },

    inputField: {
      marginVertical: 5,
      padding: 5,
      borderColor: '#000000',
      width: 200,
      borderWidth: 1
    },
  
    text: {
      color: "#ffff",
      fontWeight: "bold"
    },

    textTitle: {
        marginVertical: 50,
        fontWeight: "bold"
    },
  
  });

export default Login