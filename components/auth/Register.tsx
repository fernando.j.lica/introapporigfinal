import React, { Component } from 'react'
import { StyleSheet, View, Text, TextInput, Button, TouchableOpacity } from 'react-native';

export class Register extends Component {
    
    constructor(props) {
        super(props);

        this.state = {
            name: '',
            email: '',
            password: ''
        }

        //depois do render
        this.onSignUp = this.onSignUp.bind(this)
         
    }

    usersList = [];

    //depois do render
    onSignUp(){
        
        const { name, email, password } = this.state;

        this.usersList = this.props.route.params;
        
        if(this.usersList === undefined){
            this.usersList = [{name: 'user1', email: 'user1@user1.com', password: 'user1pass'}]
        }

        const isFound = this.usersList.some( user => { 
                //console.log(user.name, name)
                if (user.email == email) {
                    return true 
                }
                else{
                    return false
                }
        });

        if(!isFound){this.usersList.push({ name, email, password })}

        //this.state.usersList = usersList

        console.log(this.usersList)
        // fireBase
    }


    render() {
        console.log('Register PARAMS: ', this.props.route.params)
        this.usersList = this.props.route.params
        return (
            <View style={styles.container}>

                <Text style={styles.textTitle}>Register Screen</Text>

                <TextInput style={styles.inputField}
                    placeholder="name"
                    onChangeText={(name) => this.setState({ name })}
                />
                <TextInput style={styles.inputField}
                    placeholder="email"
                    onChangeText={(email) => this.setState({ email })}
                />
                <TextInput style={styles.inputField}
                    placeholder="password"
                    secureTextEntry={true}
                    onChangeText={(password) => this.setState({ password })}
                />

                <TouchableOpacity style={styles.button} onPress={() => this.onSignUp()}>
                        <Text style={styles.text}>Sign Up</Text>
                </TouchableOpacity>

                <TouchableOpacity style={styles.button} onPress={() => this.props.navigation.navigate("Landing", this.props.route.params)}>
                        <Text style={styles.text}>Back</Text>
                </TouchableOpacity>
                
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      justifyContent: "center",
      alignItems: "center",
    },
    
   
    button: {
      justifyContent: "center",
      alignItems: "center",
      padding: 10,
      borderRadius: 5,
      marginTop: 15,
      width: 100,
      backgroundColor: "#3fa33c",
    },

    inputField: {
        marginVertical: 5,
        padding: 5,
        borderColor: '#000000',
        width: 200,
        borderWidth: 1
      },
    
  
    text: {
      color: "#ffff",
      fontWeight: "bold"
    },

    textTitle: {
        marginVertical: 50,
        fontWeight: "bold"
    },
  
  });

export default Register