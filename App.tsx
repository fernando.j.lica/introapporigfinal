import { React, Component } from 'react';
import { NavigationContainer } from '@react-navigation/native'
import { createStackNavigator } from '@react-navigation/stack'
import { createMaterialBottomTabNavigator } from '@react-navigation/material-bottom-tabs'

import LandingScreen from './components/auth/Landing'
import RegisterScreen from './components/auth/Register'
import LoginScreen from './components/auth/Login'
import MainMenuScreen from './components/MainMenu'
import SecondaryMenuScreen from './components/SecondaryMenu'


const Stack = createStackNavigator();
const Tab = createMaterialBottomTabNavigator();

function TabNav() {
  return (
    <Tab.Navigator 
      initialRouteName= "MainMenu"
      barStyle={{ backgroundColor: '#DFDFDF' }}
      >
      <Tab.Screen name="SecondaryMenu" component={SecondaryMenuScreen} 
        options={{
          //tabBarIcon: ({ color }) => (
          //    <MaterialCommunityIcons name="clipboard-check-outline" color={color} size={26} />
          //),
          title: 'SecondaryMenu'
      }}/>
      <Tab.Screen name="MainMenu" component={MainMenuScreen} //listeners={{tabPress: (e) => { console.log('TAB') },}}
        options={{
          //tabBarIcon: ({ color }) => (
          //    <MaterialCommunityIcons name="home" color={color} size={26} />
          //),
          title: 'MainMenu'
      }}/>

    </Tab.Navigator>

  )

}



export default function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="Landing">
        <Stack.Screen name="Landing" component={LandingScreen} options= {{headerShown: false}}/>
        <Stack.Screen name="Register" component={RegisterScreen} options= {{headerShown: false}}/>
        <Stack.Screen name="Login" component={LoginScreen} options= {{headerShown: false}}/>
        <Stack.Screen name="MainMenu" component={TabNav} options= {{headerShown: false}}/>
      </Stack.Navigator>
    </NavigationContainer>
  );
}

{/* <Stack.Screen name="MainMenu" component={MainMenuScreen} options= {{headerShown: false}}/> */}

/*
export class App extends Component {
  constructor(props){
    super(props);
    this.state = {
      loaded: false,
    }
  }


  componentDidMount(){

  }

  render() {
    return (
      <NavigationContainer>
      <Stack.Navigator initialRouteName="Landing">
        <Stack.Screen name="Landing" component={LandingScreen} options= {{headerShown: false}}/>
        <Stack.Screen name="Register" component={RegisterScreen} options= {{headerShown: false}}/>
      </Stack.Navigator>
    </NavigationContainer>
    )
  }
}
*/


